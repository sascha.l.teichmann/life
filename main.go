// This is Free Software licensed under the Apache License, Version 2.0 (the "License").
//
// Copyright 2020 by the contributors.
// See CONTRIBUTORS for details.
//
// SPDX-License-Identifier: Apache-2.0
// License-Filename: LICENSES/apache-2.0.txt
//
// Author(s):
//  * Sascha L. Teichmann (sascha.teichmann@intevation.de)

package main

import (
	"flag"
	"fmt"
	"math/rand"
	"time"

	"log"

	"github.com/veandco/go-sdl2/sdl"
)

type game struct {
	columns int
	rows    int
	cells   []byte
	tmp     []byte
}

func main() {
	var (
		width      int
		height     int
		resolution int
		fps        float64
	)
	flag.IntVar(&width, "width", 1000, "width of the output window")
	flag.IntVar(&height, "height", 600, "height of the output window")
	flag.IntVar(&resolution, "resolution", 20, "number of pixels width/height per cell")
	flag.Float64Var(&fps, "fps", 15, "Limit FPS. Negative value means unlimited.")

	flag.Parse()

	columns, rows := width/resolution, height/resolution

	g := newGame(columns, rows)
	g.random()

	var err error
	sdl.Main(func() {
		err = g.run(width, height, resolution, fps)
	})
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func newGame(columns, rows int) *game {
	size := columns * rows
	return &game{
		columns: columns,
		rows:    rows,
		cells:   make([]byte, size),
		tmp:     make([]byte, size),
	}
}

func (g *game) random() {
	rnd := rand.New(rand.NewSource(time.Now().Unix()))

	for i := 0; i < g.rows; i++ {
		for j := 0; j < g.columns; j++ {
			if rnd.Intn(2) == 1 {
				g.set(j, i)
			}
		}
	}
}

func (g *game) set(x, y int) {

	cells, columns := g.cells, g.columns

	var left, right, above, below int

	if x == 0 {
		left = columns - 1
	} else {
		left = -1
	}
	if x == columns-1 {
		right = -(columns - 1)
	} else {
		right = 1
	}
	if y == 0 {
		above = len(cells) - columns
	} else {
		above = -columns
	}
	if y == g.rows-1 {
		below = -(len(cells) - columns)
	} else {
		below = columns
	}

	ofs := y*columns + x

	cells[ofs] |= 1

	cells[ofs+above+left] += 0x2
	cells[ofs+above] += 0x2
	cells[ofs+above+right] += 0x2
	cells[ofs+left] += 0x2
	cells[ofs+right] += 0x2
	cells[ofs+below+left] += 0x02
	cells[ofs+below] += 02
	cells[ofs+below+right] += 0x2
}

func (g *game) unset(x, y int) {

	cells, columns := g.cells, g.columns

	var left, right, above, below int

	if x == 0 {
		left = columns - 1
	} else {
		left = -1
	}
	if x == columns-1 {
		right = -(columns - 1)
	} else {
		right = 1
	}
	if y == 0 {
		above = len(cells) - columns
	} else {
		above = -columns
	}
	if y == g.rows-1 {
		below = -(len(cells) - columns)
	} else {
		below = columns
	}

	ofs := y*columns + x

	cells[ofs] &= 0xfe

	cells[ofs+above+left] -= 0x2
	cells[ofs+above] -= 0x2
	cells[ofs+above+right] -= 0x2
	cells[ofs+left] -= 0x2
	cells[ofs+right] -= 0x2
	cells[ofs+below+left] -= 0x02
	cells[ofs+below] -= 02
	cells[ofs+below+right] -= 0x2
}

func (g *game) all(draw func(int, int)) {
	for i, row := 0, 0; i < g.rows; i, row = i+1, row+g.columns {
		for j, c := range g.cells[row : row+g.columns] {
			if c&1 == 1 {
				draw(j, i)
			}
		}
	}
}

func (g *game) next(draw func(int, int)) {

	copy(g.tmp, g.cells)

	for i, row := 0, 0; i < g.rows; i, row = i+1, row+g.columns {
		for j, c := range g.tmp[row : row+g.columns] {
			if c == 0 {
				continue
			}
			n := c >> 1
			if c&0x1 == 1 {
				if n == 2 || n == 3 {
					draw(j, i)
				} else {
					g.unset(j, i)
				}
			} else {
				if n == 3 {
					g.set(j, i)
					draw(j, i)
				}
			}
		}
	}
}

func (g *game) render(renderer *sdl.Renderer, cells func(func(int, int))) {
	ow, oh, _ := renderer.GetOutputSize()
	w := ow / int32(g.columns)
	h := oh / int32(g.rows)

	renderer.SetDrawColor(0xff, 0xff, 0xff, 0xff)

	// Fast path for pixels.
	if w == 1 && h == 1 {
		const bufSize = 64
		pts := make([]sdl.Point, 0, bufSize)

		cells(func(x, y int) {
			pts = append(pts, sdl.Point{X: int32(x), Y: int32(y)})
			if len(pts) == bufSize {
				renderer.DrawPoints(pts)
				pts = pts[:0]
			}
		})
		if len(pts) > 0 {
			renderer.DrawPoints(pts)
		}
		return
	}

	const bufSize = 16
	rts := make([]sdl.Rect, 0, bufSize)

	cells(func(x, y int) {
		rts = append(rts, sdl.Rect{
			X: int32(x) * w,
			Y: int32(y) * h,
			W: w - 1,
			H: h - 1})
		if len(rts) == bufSize {
			renderer.FillRects(rts)
			rts = rts[:0]
		}
	})
	if len(rts) > 0 {
		renderer.FillRects(rts)
	}
}

func (g *game) run(width, height, resolution int, fps float64) error {

	var window *sdl.Window
	var err error

	sdl.Do(func() {
		window, err = sdl.CreateWindow(
			"Game of Life",
			sdl.WINDOWPOS_UNDEFINED,
			sdl.WINDOWPOS_UNDEFINED,
			int32(width), int32(height),
			sdl.WINDOW_SHOWN)
	})

	if err != nil {
		return err
	}
	defer sdl.Do(func() { window.Destroy() })

	var renderer *sdl.Renderer

	sdl.Do(func() {
		renderer, err = sdl.CreateRenderer(window, -1, sdl.RENDERER_ACCELERATED)
	})

	if err != nil {
		return err
	}
	defer sdl.Do(func() { renderer.Destroy() })

	sdl.Do(func() {
		renderer.SetDrawColor(0x00, 0x00, 0x00, 0xff)
		renderer.Clear()
		g.render(renderer, g.all)
		renderer.Present()
	})

	last := time.Now()
	for {
		sdl.Do(func() {
			renderer.SetDrawColor(0x00, 0x00, 0x00, 0xff)
			renderer.Clear()
			g.render(renderer, g.next)
			renderer.Present()
		})
		var quit bool

		sdl.Do(func() {
			for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
				switch event.(type) {
				case *sdl.QuitEvent:
					quit = true
				}
			}
		})

		if quit {
			break
		}

		now := time.Now()
		diff := now.Sub(last)
		mfps := float64(time.Second / diff)
		if fps > 0 {
			if fps < mfps {
				n := mfps / fps
				sleep := time.Duration(n-1) * diff
				now = now.Add(sleep)
				time.Sleep(sleep)
			} else if fps > mfps {
				fmt.Printf("Missing target fps: %.2f\n", mfps)
			}
		} else {
			fmt.Printf("fps %.2f\n", mfps)
		}
		last = now
	}
	return nil
}
